package com.kulaev.spring.boot.spring_cafe.payload.response;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
public class InvalidLoggingResponse {
    private String username;
    private String password;

    public InvalidLoggingResponse() {
        this.username = "Invalid Username";
        this.password =  "Invalid Password";
    }
}
