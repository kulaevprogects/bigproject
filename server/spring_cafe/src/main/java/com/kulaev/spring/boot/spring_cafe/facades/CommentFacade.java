package com.kulaev.spring.boot.spring_cafe.facades;

import com.kulaev.spring.boot.spring_cafe.dto.CommentDTO;
import com.kulaev.spring.boot.spring_cafe.entity.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentFacade {
    public CommentDTO commentToNewCommentDTO(Comment comment){
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setUsername(comment.getUsername());
        commentDTO.setMessage(comment.getMessage());
        return commentDTO;
    }
}
