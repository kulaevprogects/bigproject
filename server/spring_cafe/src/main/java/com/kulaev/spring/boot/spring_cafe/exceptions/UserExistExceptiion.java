package com.kulaev.spring.boot.spring_cafe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserExistExceptiion extends RuntimeException {
    public UserExistExceptiion(String message) {
        super(message);
    }
}
