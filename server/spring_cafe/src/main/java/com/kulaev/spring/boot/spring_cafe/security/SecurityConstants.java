package com.kulaev.spring.boot.spring_cafe.security;

public class SecurityConstants {
    public static final String SIFN_UP_URLS = "/api/auth/*";;
    public static final String SERCRET = "SecretKeyGenJWT";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String CONTEXT_TYPE = "application/json";
    public static final long EXPIRATION_TIME = 600000;
}
