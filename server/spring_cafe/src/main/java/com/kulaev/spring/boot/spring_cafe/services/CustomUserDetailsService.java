package com.kulaev.spring.boot.spring_cafe.services;

import com.kulaev.spring.boot.spring_cafe.entity.User;
import com.kulaev.spring.boot.spring_cafe.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findUserByEmail(username).orElseThrow(()-> new UsernameNotFoundException("User not found with " +
                "username "+username));
        return build(user);
    }
    public User loadUserById(Long id){
        User user = userRepository.findUserById(id).orElse(null);
        return user;
    }
    public static User build(User user){
        List<GrantedAuthority> authorities =  user.getRoles().stream().map(eRole -> new SimpleGrantedAuthority(eRole.name()))
                .collect(Collectors.toList());
        return  new User(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }
}
