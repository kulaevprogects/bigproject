package com.kulaev.spring.boot.spring_cafe.validations;

import com.kulaev.spring.boot.spring_cafe.annotations.PasswordMatches;
import com.kulaev.spring.boot.spring_cafe.payload.request.SignUpRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches,Object> {
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        SignUpRequest signUpRequest = (SignUpRequest) obj;
        return signUpRequest.getPassword().equals(signUpRequest.getConfirmPassword());
    }
}
