package com.kulaev.spring.boot.spring_cafe.dto;

import lombok.Data;

import java.util.Set;
@Data
public class PostDTO {
    private Long id;
    private String title;
    private String caption;
    private String location;
    private String username;
    private Integer likes;
    private Set<String> usersliked;
}
