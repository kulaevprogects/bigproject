package com.kulaev.spring.boot.spring_cafe.web;

import com.kulaev.spring.boot.spring_cafe.entity.ImageModel;
import com.kulaev.spring.boot.spring_cafe.payload.response.MessageResponse;
import com.kulaev.spring.boot.spring_cafe.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping("/api/image")
@CrossOrigin
public class ImageController {
    @Autowired
    private ImageService imageService;
    @RequestMapping("/update")
    public ResponseEntity<MessageResponse> uploadImageToUSer(@RequestParam("file")MultipartFile file, Principal principal) throws
            IOException{
        imageService.uploadImageToUser(file,principal);
        return ResponseEntity.ok(new MessageResponse("Image was sucessfully uploaded"));
    }
    @RequestMapping("{postId}/upload")
    public ResponseEntity<MessageResponse> uploadeImageToPost(@RequestParam("file")MultipartFile file,@PathVariable("postId") String postId, Principal principal) throws
    IOException{
        imageService.uploadImageToPost(file,principal, Long.parseLong(postId));
        return ResponseEntity.ok(new MessageResponse("Image was sucessfully uploaded"));
    }
    @GetMapping("/profileImage")
    public ResponseEntity<ImageModel> getImageForUser(Principal principal){
        ImageModel userImage = imageService.getImageToUser(principal);
        return new ResponseEntity<>(userImage,HttpStatus.OK);
    }
    @RequestMapping("{postId}/image")
    public ResponseEntity<ImageModel> getImageToPost(@PathVariable("postId") String postId)
{
        ImageModel userImage = imageService.getImageToPost(Long.parseLong(postId));
        return new ResponseEntity<>(userImage,HttpStatus.OK);
    }
}
