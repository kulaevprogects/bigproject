package com.kulaev.spring.boot.spring_cafe.services;

import com.kulaev.spring.boot.spring_cafe.dto.PostDTO;
import com.kulaev.spring.boot.spring_cafe.entity.ImageModel;
import com.kulaev.spring.boot.spring_cafe.entity.Post;
import com.kulaev.spring.boot.spring_cafe.entity.User;
import com.kulaev.spring.boot.spring_cafe.exceptions.PostNotFoundException;
import com.kulaev.spring.boot.spring_cafe.repository.ImageRepository;
import com.kulaev.spring.boot.spring_cafe.repository.PostRepository;
import com.kulaev.spring.boot.spring_cafe.repository.UserRepository;
import com.kulaev.spring.boot.spring_cafe.security.JWTAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {
    public static final Logger LOG = LoggerFactory.getLogger(PostService.class);
    private  final PostRepository postRepository;
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;
    @Autowired
    public PostService(PostRepository postRepository, UserRepository userRepository, ImageRepository imageRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
    }
    public Post createPost(PostDTO postDTO, Principal principal){
        User user = getUserFromPrincipal(principal);
        Post post = new Post();
        post.setUser(user);
        post.setCaption(postDTO.getCaption());
        post.setLocation(postDTO.getLocation());
        post.setTitle(postDTO.getTitle());
        post.setLikes(0);
        LOG.info("Saving Post for User{}", user.getEmail());
        return postRepository.save(post);
    }
    public List<Post> getAllPosts(){
        return postRepository.findAllByOrderByCreatedDateDesc();
    }
    public List<Post> getAllPostsForUser(Principal principal){
        User user = getUserFromPrincipal(principal);
        return postRepository.findAllByUserOrderByCreatedDateDesc(user);
    }
    public Post likePost(Long PostId, String username){
        Post post = postRepository.findById(PostId)
                .orElseThrow(()-> new PostNotFoundException("Post cannot" + " be found "));
        Optional<String> userLiked = post.getLikedUsers()
                .stream().filter(u-> u.equals(username)).findAny();
        if (userLiked.isPresent()){
            post.setLikes(post.getLikes() - 1);
            post.getLikedUsers().remove(username);
        }
        else {
            post.setLikes(post.getLikes() + 1);
            post.getLikedUsers().add(username);
        }
               return postRepository.save(post);
    }
    public Post findPostByUserandId(Long PostId, Principal principal){
        User user = getUserFromPrincipal(principal);
        return postRepository.findPostByIdAndUser(PostId,user).orElseThrow(()-> new PostNotFoundException("Post cannot" +
                " be found for Username" + user.getEmail()));
    }
    public void deletePost(Long postId, Principal principal){
        Post post = findPostByUserandId(postId,principal);
        Optional<ImageModel> imageModel = imageRepository.findByPostId(postId);
        postRepository.delete(post);
        imageModel.ifPresent(imageRepository::delete);
    }
    private User getUserFromPrincipal(Principal principal){
        String username = principal.getName();
        return userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found with the username" + username));
    }
}
