package com.kulaev.spring.boot.spring_cafe.services;

import com.kulaev.spring.boot.spring_cafe.dto.UserDTO;
import com.kulaev.spring.boot.spring_cafe.entity.User;
import com.kulaev.spring.boot.spring_cafe.entity.enums.ERole;
import com.kulaev.spring.boot.spring_cafe.exceptions.UserExistExceptiion;
import com.kulaev.spring.boot.spring_cafe.payload.request.SignUpRequest;
import com.kulaev.spring.boot.spring_cafe.repository.UserRepository;
import com.kulaev.spring.boot.spring_cafe.security.JWTAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class UserService {
    public static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private  final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    public User createUser(SignUpRequest signUpRequest){
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setName(signUpRequest.getFirstname());
        user.setLastname(signUpRequest.getLastname());
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode((signUpRequest.getPassword())));
        user.getRoles().add(ERole.ROLE_USER);
        try {
            LOG.info("Saving User {}"+ signUpRequest.getEmail());
            return userRepository.save(user);
        }
        catch (Exception e){
            LOG.error("Error during registration {}", e.getMessage());
            throw new UserExistExceptiion("The user " + user.getEmail() +" already exist.");
        }
    }
    public  User updateUser(UserDTO userDTO, Principal principal){
        User user = getUserFromPrincipal(principal);
        user.setName(userDTO.getFirstname());
        user.setLastname(userDTO.getLastname());
        user.setBiografy(userDTO.getBio());
        return userRepository.save(user);
    }
    public User gerCurrentUser(Principal principal){
        return getUserFromPrincipal(principal);
    }
    private User getUserFromPrincipal(Principal principal){
        String username = principal.getName();
        return userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found with the username" + username));
    }

    public User getUserById(long parseLong) {
        return userRepository.findUserById(parseLong)
                .orElseThrow(()-> new UsernameNotFoundException("User not found with the id" + parseLong));
    }
}
