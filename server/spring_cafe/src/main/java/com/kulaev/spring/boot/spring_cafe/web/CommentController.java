package com.kulaev.spring.boot.spring_cafe.web;

import com.kulaev.spring.boot.spring_cafe.dto.CommentDTO;
import com.kulaev.spring.boot.spring_cafe.dto.PostDTO;
import com.kulaev.spring.boot.spring_cafe.entity.Comment;
import com.kulaev.spring.boot.spring_cafe.facades.CommentFacade;
import com.kulaev.spring.boot.spring_cafe.payload.response.MessageResponse;
import com.kulaev.spring.boot.spring_cafe.services.CommentService;
import com.kulaev.spring.boot.spring_cafe.validations.ResponseErrorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/comment")
@CrossOrigin
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentFacade commentFacade;
    @Autowired
    private ResponseErrorValidator responseErrorValidator;
    @PostMapping("/{postId}/create")
    public ResponseEntity<Object> createComment(@Valid @RequestBody CommentDTO commentDTO, @PathVariable("postId") String postId,
                                                BindingResult bindingResult, Principal principal)
    {
        ResponseEntity<Object> errors = responseErrorValidator.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;
        Comment comment = commentService.saveComment(commentDTO, Long.parseLong(postId),  principal);
        CommentDTO newCommentDTO = commentFacade.commentToNewCommentDTO(comment);
        return  new ResponseEntity<>(newCommentDTO, HttpStatus.OK);
    }
    @GetMapping("/{postId}/all")
    public ResponseEntity<List<CommentDTO>> getAllCommentsForPost(@PathVariable("postId") String postId){
     List<CommentDTO> commentDTOList =commentService.getAllCommentsForPost(Long.parseLong(postId))
                .stream()
                .map(commentFacade::commentToNewCommentDTO)
                .collect(Collectors.toList());
        return  new ResponseEntity<>(commentDTOList, HttpStatus.OK);
    }
    @PostMapping("/{commentId}/delete")
    public  ResponseEntity<MessageResponse> deletePost(@PathVariable("commentId") String commentId){
        commentService.deleteComment(Long.parseLong(commentId));
        return new ResponseEntity<>(new MessageResponse("Comment was killed"),HttpStatus.OK);
    }
}
