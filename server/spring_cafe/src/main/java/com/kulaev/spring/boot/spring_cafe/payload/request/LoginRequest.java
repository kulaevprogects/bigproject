package com.kulaev.spring.boot.spring_cafe.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginRequest {
    @NotEmpty(message = "Username cannot be empty")
    public String username;
    @NotEmpty(message = "Paswword cannot be empty")
    public String password;

}
