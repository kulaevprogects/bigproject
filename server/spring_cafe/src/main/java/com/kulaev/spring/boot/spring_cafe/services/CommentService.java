package com.kulaev.spring.boot.spring_cafe.services;

import com.kulaev.spring.boot.spring_cafe.dto.CommentDTO;
import com.kulaev.spring.boot.spring_cafe.entity.Comment;
import com.kulaev.spring.boot.spring_cafe.entity.Post;
import com.kulaev.spring.boot.spring_cafe.entity.User;
import com.kulaev.spring.boot.spring_cafe.exceptions.PostNotFoundException;
import com.kulaev.spring.boot.spring_cafe.repository.CommentRepository;
import com.kulaev.spring.boot.spring_cafe.repository.PostRepository;
import com.kulaev.spring.boot.spring_cafe.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
@Service
public class CommentService {
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    public static final Logger LOG = LoggerFactory.getLogger(CommentService.class);
    @Autowired
    public CommentService(CommentRepository commentRepository, UserRepository userRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }
    public Comment saveComment(CommentDTO commentDTO, Long postId, Principal principal){
        User user = getUserFromPrincipal(principal);
        Post post = postRepository.findById(postId).orElseThrow(()-> new PostNotFoundException("Post" +
                "cannot be find with this id"));
        Comment comment = new Comment();
        comment.setPost(post);
        comment.setUserId(user.getId());
        comment.setUsername(user.getUsername());
        comment.setMessage(commentDTO.getMessage());
        LOG.info("Saving comment for Post: {}", post.getId());
        return commentRepository.save(comment);
    }
    public List<Comment> getAllCommentsForPost(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(()-> new PostNotFoundException("Post" +
                "cannot be find with this id"));
        List<Comment> comments =commentRepository.findAllByPost(post);
        return comments;
    }
    public void  deleteComment(Long commentId){
        Optional<Comment> comment =commentRepository.findById(commentId);
        comment.ifPresent(commentRepository::delete);
    }
    private User getUserFromPrincipal(Principal principal){
        String username = principal.getName();
        return userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found with the username" + username));
    }
}
