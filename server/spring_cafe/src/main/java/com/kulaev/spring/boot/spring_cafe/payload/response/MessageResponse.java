package com.kulaev.spring.boot.spring_cafe.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class MessageResponse {
    private String message;
}
