package com.kulaev.spring.boot.spring_cafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCafeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCafeApplication.class, args);
	}

}
