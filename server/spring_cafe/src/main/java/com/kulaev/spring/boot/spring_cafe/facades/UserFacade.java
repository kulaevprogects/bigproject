package com.kulaev.spring.boot.spring_cafe.facades;

import com.kulaev.spring.boot.spring_cafe.dto.UserDTO;
import com.kulaev.spring.boot.spring_cafe.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserFacade {
    public UserDTO userToNewUserDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstname(user.getName());
        userDTO.setLastname(user.getLastname());
        userDTO.setUsername(user.getUsername());
        userDTO.setBio(user.getBiografy());
        return userDTO;
    }
}
