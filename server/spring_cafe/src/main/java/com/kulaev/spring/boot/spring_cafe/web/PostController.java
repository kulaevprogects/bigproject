package com.kulaev.spring.boot.spring_cafe.web;

import com.kulaev.spring.boot.spring_cafe.dto.PostDTO;
import com.kulaev.spring.boot.spring_cafe.entity.Post;
import com.kulaev.spring.boot.spring_cafe.facades.PostFacade;
import com.kulaev.spring.boot.spring_cafe.payload.response.MessageResponse;
import com.kulaev.spring.boot.spring_cafe.services.PostService;
import com.kulaev.spring.boot.spring_cafe.validations.ResponseErrorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/post")
@CrossOrigin
public class PostController {
    @Autowired
    private PostFacade postFacade;
    @Autowired
    private PostService postService;
    @Autowired
    private ResponseErrorValidator responseErrorValidator;
    @PostMapping("/create")
    public ResponseEntity<Object> createPost(@Valid @RequestBody PostDTO postDTO, BindingResult bindingResult, Principal principal){
        ResponseEntity<Object> errors = responseErrorValidator.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;
        Post post= postService.createPost(postDTO,principal);
        PostDTO newPost = postFacade.postToNewPostDTo(post);
        return new ResponseEntity<>(newPost, HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<List<PostDTO>> getAllPosts(){
       List<PostDTO> postDTOList =postService.getAllPosts()
               .stream()
               .map(postFacade::postToNewPostDTo)
               .collect(Collectors.toList());
       return  new ResponseEntity<>(postDTOList,HttpStatus.OK);
    }
    @GetMapping("/user/posts")
    public ResponseEntity<List<PostDTO>> getAllPostsForUser(Principal principal){
        List<PostDTO> postDTOList =postService.getAllPostsForUser(principal)
                .stream()
                .map(postFacade::postToNewPostDTo)
                .collect(Collectors.toList());
        return  new ResponseEntity<>(postDTOList,HttpStatus.OK);
    }
    @PostMapping("/{postId}/{username}/like")
    public ResponseEntity<PostDTO> likePost(@PathVariable("postId") String postId, @PathVariable("username") String username
    ){
        Post post = postService.likePost(Long.parseLong(postId),username);
        PostDTO postDTO = postFacade.postToNewPostDTo(post);
        return new ResponseEntity<>(postDTO,HttpStatus.OK);
    }
    @PostMapping("/{postId}/delete")
    public  ResponseEntity<MessageResponse> deletePost(@PathVariable("postId") String postId, Principal principal){
        postService.deletePost(Long.parseLong(postId),principal);
        return new ResponseEntity<>(new MessageResponse("Post was killed"),HttpStatus.OK);
    }
}
