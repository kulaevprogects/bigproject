import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../models/Post";
import {Observable} from "rxjs";
const POST_API = "http://185.174.235.90:8082/api/post/"
@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }
  createPost(post: Post): Observable<any> {
    return this.http.post(POST_API + 'create', post);
  }
  getAllPosts(): Observable<any>{
    return this.http.get(POST_API+'all');
  }
  getAllPostsForUser(): Observable<any>{
    return this.http.get(POST_API+'user/posts');
  }
  delete (id: number): Observable<any>{
    return this.http.post(POST_API+id+'/delete',null);
  }
  likePost(id: number, username:String):Observable<any>{
    return this.http.post(POST_API+id+'/'+username+'/like',null)
  }
}
