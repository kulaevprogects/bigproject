import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Post} from "../models/Post";
const IMAGE_API = "http://185.174.235.90:8082/api/image/"
@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient) { }
  uploadImageToUser(file: File): Observable<any>{
    const uploadData = new FormData();
    uploadData.append('file',file)
    return this.http.post(IMAGE_API+'update',uploadData)
  }
  uploadImageToPost(file: File, postId: number): Observable<any>{
    const uploadData = new FormData();
    uploadData.append('file',file)
    return this.http.post(IMAGE_API+postId+'/upload',uploadData)
  }
  getProfileImage(): Observable<any>{
    return this.http.get(IMAGE_API+ 'profileImage')
  }

  getImageForPost(postId: number | undefined): Observable<any>{
    return this.http.get(IMAGE_API + postId +'/image')
  }

}
