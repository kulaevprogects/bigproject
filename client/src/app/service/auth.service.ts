import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
const AUTH_API = "http://185.174.235.90:8082/api/auth/"
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  public login(user: any): Observable<any>{
    return this.http.post(AUTH_API+'signin',{username: user.username,
    password: user.password})
  }
  public register(user: any):Observable<any>{
    return this.http.post(AUTH_API+'signup',{username: user.username,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      password: user.password,
      confirmPassword: user.confirmPassword})
  }
}
